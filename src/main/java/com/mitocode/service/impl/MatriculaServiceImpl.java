package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.document.Matricula;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IMatriculaRepo;
import com.mitocode.service.IMatriculaService;

@Service
public class MatriculaServiceImpl extends ICRUDImpl<Matricula, String> implements IMatriculaService{

	@Autowired
	private IMatriculaRepo repo;
	
	@Override
	protected IGenericRepo<Matricula, String> getRepo(){
		return repo;
	}

	/*@Override
	public Flux<Cliente> listarDemorado() {
		return repo.findAll().repeat(10).delayElements(Duration.ofSeconds(1));
	}

	@Override
	public Flux<Cliente> listarSobreCargado() {
		return repo.findAll().repeat(500);
	}*/
}
