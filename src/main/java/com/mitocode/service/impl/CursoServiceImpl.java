package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.document.Curso;
import com.mitocode.repo.ICursoRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.ICursoService;

@Service
public class CursoServiceImpl extends ICRUDImpl<Curso, String> implements ICursoService{

	@Autowired
	private ICursoRepo repo;
	
	@Override
	protected IGenericRepo<Curso, String> getRepo(){
		return repo;
	}

	/*@Override
	public Flux<Cliente> listarDemorado() {
		return repo.findAll().repeat(10).delayElements(Duration.ofSeconds(1));
	}

	@Override
	public Flux<Cliente> listarSobreCargado() {
		return repo.findAll().repeat(500);
	}*/
}
